#!/usr/bin/env python3

import random
# Say we want to initialize a list of 20 random integers between 0 and 99.
# We'll use the python "random" module to do this.

# The long way to do this would be to initialize an empty list, then use
# a for loop to append each element.
rand_list1 = []
for ii in range(20):
    rand_list1.append(random.randrange(100))
    # random.randrange(100) gives us a random integer from 0 to 99.
    # The syntax is like range().
print(rand_list1)
# Note - at this point ii == 19

# The same thing done using a list comprehension would be
rand_list2 = [random.randrange(100) for jj in range(20)]
print(rand_list2)
# Note - at this point jj is undefined. It only has a value within the list
# comprehension statement.

# List comprehensions can iterate over more than one variable. So we could
# initialize some set of coordinates (here as tuples) as follows.
xy_list1 = [(x, y) for x in range(6) for y in range(3)]
print(xy_list1)

# We can also add if statements to omit some values, so that quite complex
# constructions can be used in a compact but readable way.
xy_list2 = [x / (x-y) for x in range(6) for y in range(3) if x != y]
print(xy_list2)
