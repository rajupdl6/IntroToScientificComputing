#!/usr/bin/env python3

# Variables defined outside any function are called global variables. We can
# access their values within all functions. However to a assign a value to a
# global variable within a function we need to explicitly flag them with the
# "global" keyword.
a = 1

def increase_a(b):
    """ Increase the value of the global a by the input value of b."""
    # The variables defined within a function are local only. If we try to
    # use a variable both as a parameter and a global within a function
    # python will generate an error.
    global a
    a = a + b
    return

def main():

    c = 2
    increase_a(c)
    print(a)
    return

if __name__ == '__main__':
    # This is outside any function definition also, so the value of a can
    # simply be updated here.
    a = 20
    main()
