Python - 2. More Advanced Topics
================================

[Full topic list](../README.md)

You'll be able to get quite far just using the commands and functionality
given earlier in the [previous section](../python1/README.md). If you want to
refresh this material from an alternative perspective, I suggest going through
the first 7 sections of the [python
tutorial](https://docs.python.org/3/tutorial/index.html).

As you progress you may find some of the following more advanced functionality
quite useful.

List Comprehensions
-------------------

Python offers a nice compact way to create and populate lists, using what is
called "list comprehension". This is also usually the fastest way to do so.

An example of this is given in `list_comp.py`:

```python
#!/usr/bin/env python3

import random
# Say we want to initialize a list of 20 random integers between 0 and 99.
# We'll use the python "random" module to do this.

# The long way to do this would be to initialize an empty list, then use
# a for loop to append each element.
rand_list1 = []
for ii in range(20):
    rand_list1.append(random.randrange(100))
    # random.randrange(100) gives us a random integer from 0 to 99.
    # The syntax is like range().
print(rand_list1)
# Note - at this point ii == 19

# The same thing done using a list comprehension would be
rand_list2 = [random.randrange(100) for jj in range(20)]
print(rand_list2)
# Note - at this point jj is undefined. It only has a value within the list
# comprehension statement.

# List comprehensions can iterate over more than one variable. So we could
# initialize some set of coordinates (here as tuples) as follows.
xy_list1 = [(x, y) for x in range(6) for y in range(3)]
print(xy_list1)

# We can also add if statements to omit some values, so that quite complex
# constructions can be used in a compact but readable way.
xy_list2 = [x / (x-y) for x in range(6) for y in range(3) if x != y]
print(xy_list2)
```

You can see above that temporary variables used to populate the list are not
available in the main code body. This is due to the scope rules as mentioned
in the previous section. Note that this is not the case in python2.

Scope
-----

Scope determines which value is used for a variable or object where they may
be defined within several different levels of the code. The full rules for how
python determines this are given in [the python language
reference](https://docs.python.org/3/reference/executionmodel.html#resolution-of-names),
and also described in a more digestible way in [section 9 of the python
tutorial](https://docs.python.org/3/tutorial/classes.html#python-scopes-and-namespaces).
An example showing how these rules are important is given in `scope.py`:

```python
#!/usr/bin/env python3

# Variables defined outside any function are called global variables.
a = 1

def increase_a(a):
    """ Increase the value of the argument by 1"""
    a = a + 1
    return a

def main():

    a = 10
    a = increase_a(a)
    # Can you guess what will be output here?
    print(a)

if __name__ == '__main__':
    a = 20
    main()
```

Generally though, the thing to remember is that variables defined within a
function are only visible within that function.

It is possible to define and use global variables which are available within
all functions, but this can make your code difficult to understand so should
be done with caution. The example `scope2.py` shows how these may be used:

```python
#!/usr/bin/env python3

# Variables defined outside any function are called global variables. We can
# access their values within all functions. However to a assign a value to a
# global variable within a function we need to explicitly flag them with the
# "global" keyword.
a = 1

def increase_a(b):
    """ Increase the value of the global a by the input value of b."""
    # The variables defined within a function are local only. If we try to
    # use a variable both as a parameter and a global within a function
    # python will generate an error.
    global a
    a = a + b
    return

def main():

    c = 2
    increase_a(c)
    print(a)
    return

if __name__ == '__main__':
    # This is outside any function definition also, so the value of a can
    # simply be updated here.
    a = 20
    main()
```

Classes
-------

You can make your own
[classes](https://docs.python.org/3/tutorial/classes.html) in python. Usually
these are used to collect together a set of related function definitions and
other associated statements. They are the core component of object-oriented
programming in python. You can uses classes to take a set of related functions
and data and group them together in a container. For example a class could be
used to store data representing some object being simulated, and contain
functions that are used modify the data associated with that object. In
practice you can think of them as something like mini modules. An example of a
code which creates and uses a class to represent atoms in a simple 1D
simulation is given in `atom_class.py`:

```python
#!/usr/bin/env python3

class Atom:
    """Define details of atoms in our 1D simulation."""
    def __init__(self, species, mass, position, velocity):
        """Initialise the atom class.

        The atomic species, mass (amu), position (Angstrom) and velocity
        (Angstrom/ps) must be specified. The force is always initialized to
        zero.

        """
        self.species = species
        self.mass = mass
        self.position = position
        self.velocity = velocity
        self.force = 0.0

    def update_position(self, timestep):
        """Update the position using its velocity and the timestep (ps)."""
        self.position = self.position + timestep * self.velocity

    def update_velocity(self, timestep):
        """Update the velocity given force on it and timestep."""
        self.velocity = self.velocity + self.force/self.mass*timestep

    def set_force(self, atom_list, force_func):
        """Find the force on this atom.

        This is found from a list of atoms and a function for calculating the
        force between two atoms. Recall we can pass functions as arguments
        just as variables or any other objects.

        """
        self.force = 0.0
        for atom2 in atom_list:
            # We don't want to include a force from the atom on itself.
            if self != atom2:
               self.force = self.force + force_func(self.position,
                       atom2.position)

def harmonic_force(pos1, pos2, k=343415.05, equil=0.74):
    """Return a harmonic force on pos1 given pos2."""
    # Two optional parameters above are specified by adding default values, so
    # they don't need to be specified when the function is called.
    # We use a very simple force function here.
    if pos1 < pos2:
        equil = - equil
        # This gives the force in the correct direction whether the atom is
        # to the left or right of the other.
    return -k * (pos1 - pos2 - equil)

def get_atoms():
    """Return a list of atom objects representing our simulated atoms."""
    # In a real simulation this information would be read from a file.
    atom_list = []
    # Lists can be composed of class instances.
    atom_list.append(Atom('H', 1.0, 0.0, 0.0))
    # We could also refer to paramter names explicitly. The order doesn't
    # matter if we do so.
    atom_list.append(Atom(mass=1.0, species='H', position=0.9, velocity=0.0))
    return atom_list

def output_positions(atom_list, time):
    """Output all atomic positions along with the current time."""
    # We use end=' ' to suppress the newline but add a space, so all positions
    # are on one line for each timestep.
    print(time, end=' ')
    for atom in atom_list:
        print(atom.position, end=' ')
    print()

def run_simulation(atoms, timestep, nsteps, force_func):
    """Simulate the evolution of atomic positions under force_func,"""
    # Start by outputting the initial positions.
    time = 0.0
    output_positions(atoms, time)
    for i_t in range(nsteps):
        time = time + timestep
        # First update all the velocities.
        for atom in atoms:
            atom.set_force(atoms, harmonic_force)
            atom.update_velocity(timestep)
        # Then update all the positions.
        for atom in atoms:
            atom.update_position(timestep)
        # And output the new positions.
        output_positions(atoms, time)

def main():
    atom_list = get_atoms()
    run_simulation(atoms=atom_list, timestep=0.0001, nsteps=1000,
            force_func=harmonic_force)

if __name__ == '__main__':
    main()
```

While this is the longest example so far, you can see that it is really just
composed of a set of fairly simple functions. The atom class definition at the
top includes four functions, the first of which `__init__` sets the internal
class variables when the function is first defined, and the rest are used to
modify these variables in some way. If you want to plot the atomic positons
vs time, you could redirect the output from this to a file with
`./atom_class.py > pvt.dat`, and try visualizing it with gnuplot. We'll
cover how to do this within python itself later in the course.

Using Arguments in your Python Script
-------------------------------------

Python comes with a module that makes adding command line arguments to your
code very simple. This module is called
[`argparse`](https://docs.python.org/3.6/library/argparse.html). It's worth
looking at the online [tutorial](https://docs.python.org/3.6/howto/argparse.html)
if you plan to use this. A simple example is given in `args.py`:

```python
#!/usr/bin/env python3

import argparse

def main():
    # First we initialize the parser. This is an object which will store
    # details of our arguments.
    parser = argparse.ArgumentParser()
    # Then we add each argument we want our code to support. There are several
    # options here we can specify, such as "help" which adds help text for the
    # argument. This will add a positional argument.
    parser.add_argument("datafile", help="The filename for the datafile.")
    # We can also add optional arguments and specify both long and short flags
    # along with the type and a default value.
    parser.add_argument("-t", "--threshold", type=float, default=1.0e-6,
            help="Values are considered zero below this threshold.")
    # Once we're finished, we parse the arguments passed to the code and
    # store them.
    args = parser.parse_args()

    print("The value of datafile is", args.datafile)
    print("The value of threshold is", args.threshold)

if __name__ == "__main__":
    main()
```

Try running this code in the following ways and see what it outputs:

```bash
./args.py
./args.py -h
./args.py filename
./args.py -t 1.0e-5 filename
```

Installing Additional Packages
------------------------------

There is a huge number of modules and packages available to use with python.
If you're using the Anaconda distribution, you already have a large number
of these, but you'll often find useful modules suggested online as ways
to solve whatever issue you're having.

There are several scientific packages coded in python that you may end up
using at some point, such as `phonopy` (for calculating phonon properties),
`pythtb` (tight binding code), or `gpaw` (DFT code).

There are a large number of ways to get the package you want installed on
your system. These are outlined in detail in the [python
docs](https://docs.python.org/3/installing/), but to summarize:

- If you have root or sudo access, and the package is available on the package
  manager repositories of the OS you are using it can be easiest to install
  the package system wide in the this way. E.g. on Ubuntu variants you could
  do `sudo apt install python3-numpy` to install the numpy package for
  python 3 and its dependencies.
- Often you will not have the package you need available in system
  repositories. In this case, the easiest thing to do is to install it with
  the built-in package management system, `pip`. In recent python3 versions
  this can be used as a default module, by invoking python with `-m pip`. For
  example `sudo python3 -m pip install pythtb` will install the pythtb package
  for python3 and all it's dependencies system wide. You can search for
  packages with e.g. `python3 -m pip search pythtb`. Often there'll be a
  version of `pip` installed system wide that can be called directly as e.g.
  `pip3 install pythtb` to install the python3 package.
- You'll end up working on many systems where you don't have root or sudo
  access. You can install things within your user account with pip by adding
  the `--user` flag. This will put the executables in the `~/.local/bin`
  directory of your account. For example `sudo pip3 install --user phonopy`
  will install phonopy and put the executable in `~/.local/bin/phonopy`.
- If you download a package and want to install it manually, you can usually
  use the setup script provided. You may need to ensure yourself that you are
  meeting any dependencies for the installation. Again the `--user` flag can
  be added to install to your user account. For example, if you download the
  latest phonopy version, you could unpack it, enter the directory and type
  `python setup.py install --user` to install it to your user account.

Virtual Environments
--------------------

Recent python versions have an in-built way to create what are called "virtual
environments". These can be very useful when developing code that depends on
some non-standard modules, or if you're testing different versions of a
module, or just want to keep your system or user account clear of python
packages. A virtual environment is a directory in which you tell python to
store all its environment settings and packages. To create and start using a
virtual environment you can do the following for example:

```bash
mkdir TestEnv
python3 -m venv TestEnv
source TestEnv/bin/activate
```

Once you do this, you'll notice your prompt changes to be prefixed by
`(TestEnv)`. At this point, `python` will now point to whatever version
was used to create the virtual environment, and `pip` will be the correct
version for this python version (whether for python 2 or 3). Any packages
installed through `pip` (and without the `--user` flag) will now be
installed to the TestEnv directory, and will be automatically in your path as
long as the virtual environment is activated.

To deactivate the environment you can just type `deactivate`. You'll notice
your command prompt reverts to its default.

To reactivate the environment, you need to source the activate script again,
and any modules you installed there will again be available.

If you no longer need the environment, you can remove it by deleting the
folder that contains it.

Catching Exceptions
-------------------

You may have noticed in some of the examples given earlier, such as
`ifelse.py`, if a user enters a value of an unexpected type then the program
will crash with an error. Python gives you an in-built way to detect these
kinds of errors while the code is running so that you can attempt to handle
them yourself rather than having the code just crash. This is done using the
[`try`](https://docs.python.org/3/reference/compound_stmts.html#try)
statement. An example of how this can be used is given in `tryexcept.py`:

```python
#!/usr/bin/env python3

val = input("Enter an integer: ")
try:
    # This block contains the command to try.
    a = int(val)
    # val has been read as a string so we try to convert it to an integer.
except:
    # If code "try" block generates an error, this block is executed.
    print(val, "could not be converted to an integer.")
else:
    # If the code in the "try" block succeeds, this block is executed.
    if a > 10:
        print(a, "is greater than 10.")
    else:
        print(a, " is not greater than 10.")
```

This is a modification of `ifelse.py` to give the user a helpful message if
they have failed to input an integer.

Assert
------

Another way to try to catch unexpected behaviour and return a useful error
message is using the
[`assert`](https://docs.python.org/3/reference/simple_stmts.html#the-assert-statement)
statement. This can also make your life easier when debugging your codes.
For example, if you wrote a function that at some positive real number as an
argument. An example of this is in `assert_inverse.py`:

```python
#!/usr/bin/env python3

import sys

def inverse(num):
    """Return the inverse of a number."""
    # Use the "sys" module to find what the smallest float is. You could
    # also just use the smallest number you think is realistic for whatever
    # you're trying to do.
    assert abs(num) > sys.float_info.min, \
            "inverse() can only be called with non-zero argument"
    return 1.0 / num

test_nums = 1, 1.1, 0.4, 0.1, 0.0
for num in test_nums:
    print("The inverse of", num, "is", inverse(num))
```

It's good to get into the habit of including these where you can. Particularly
if you have functions that will break if arguments take certain values, even
if you're sure that will never happen.

Gotchas
-------

What do you think will be output by the following python code? Test it and
see.

```python
#!/usr/bin/env python3

  a = 1
  b = a
  a = 2
  print(b)

  a = [0, 1, 2]
  b = a
  a[0] = -1
  print(b)

  a = 0
  b = [a, 1, 2]
  a = 1
  print(b)

  a = [0]
  b = [a, [1]]
  a[0] = 1
  print(b)
  a = [2]
  print(b)
```
