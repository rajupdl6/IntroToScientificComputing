#!/usr/bin/env python3

# lists can be created using []. Elements can be of any type.
l1 = [0, 2, 3, 4, "a", -5.5]

# Elements can be extracted in the same way as was shown for strings.
print("The first element is", l1[0])
print("The last two elements are", l1[-2:])

# The len() function works in the same way also.
print("The number of values is", len(l1))

# We can also concatenate as with strings.
print([3, 4] + l1)

# lists are mutable, so we can change values as we like.
l1[-1] = 'hello'
print("The last element is now", l1[-1])
l1[:3] = [1, -1, 0]
print("The first four elements are now", l1[:4])

# We can also easily test whether a list contains a certain value
if 1 in l1:
    print("Yes,", 1, "is in l1.")
else:
    print("No,", 1, "is not in l1.")
