Python - 1. The Basics
======================

[Full topic list](../README.md)

Python is a flexible programming language that allows you to quickly and
easily create code to serve a wide variety of purposes. In this section of the
course, we will be quickly cover the python basics and progress to using the
python language to solve a variety of problems.

There are currently two major versions of Python in widespread use:

- Python2 (usually 2.7), which was the version in place when Python usage
  first really took off, so there is a significant amount of code out there
  that uses this version.
- Python3, which will be used in this course, contains a number of
  improvements that are not compatible with Python2. This meant that,
  unfortunately many codes were slow to make the upgrade to Python3 as this
  was a non-trivial process. There has been a lot of progress in the last
  number of years, so Python3 is clearly the present and future of the
  language.

You should mainly be aware that code that works in Python3 will likely give a
few errors in Python2 and vice versa. And on many systems, "python" will still
default to python2 rather than python3, so you will be safer calling python3
explicitly. This will be done in examples throughout this course.

In this section, while the source of any examples code discussed is quoted
here, they are all available in the [python1 directory](../python1) of the
repository where they can be viewed, modified and run directly.

Installing Python
-----------------

Python should have been pre-installed on the Linux VM on the laptops you
received at the start of the course. If you're using a different machine,
Python will be available through the package manager, although it can be
cumbersome making sure you also have the additional python packages you're
likely to need for scientific computing. An alternative way to get a python
installation complete with all the packages you're likely to need, is to use
the Anaconda distribution. This can be downloaded from
<https://www.continuum.io/downloads> for Windows, Mac and Linux, and can be
installed to your account without root permission. While all the python
packages you need have been installed on your Linux VM, you may prefer
to install python directly on your laptop in this manner.

Using Python
------------

There are many ways a python program can be run. Python is an interpreted
language. This means that in contrast to compiled languages such as C, C++ and
Fortran, python code can be run in the same manner as a shell script, without
needing a compilation step.

### Interactive use

The easiest way to use python is interactively. This is a useful way to do
quick calculations or check a section of code does what you expect.

In a terminal, type `python3` and an interactive python session will be
started. (Depending on your installation, you may get a python3 session by
typing `python`, but on many systems this still gives python2).

- At this point please type `python3` in a terminal, and if you don't get
  several lines of output giving the version along with other information,
  followed by a prompt as `>>>`, please raise your hand and let me know.
- One can, for example, enter simple calculations directly as `2 + 2`, `64 *
  64`, `1 / 6` etc.
- The previous result is stored in the special variable `_`, which is
  convenient when performing a series of calculations.
- To exit an interactive session you can press `ctrl+d`, or enter the command
  `exit()` or `quit()`.

### Writing and running a Python Code

Using your favourite text editor you can write your python code to a file and
save it. The usual extension used for python code is `.py`. To run the code
you can type e.g. `python3 mycode.py`.

### Writing and running Python like a Shell Script

If you recall the earlier section on shell scripting, the first line in a bash
shell script was `#!/bin/bash`. This told the system what command could be
used to interpret the code contained in the file when it is run directly,
having been marked as executable with `chmod`. We can write a python script in
the same way. The safest way to begin a python script to be used in this
manner is with the line `#!/usr/bin/env python3`. In this way we use the `env`
application so that the python3 from our user environment is used rather than
pointing directly to a particular python3 executable which may be located in
different places on different systems.

### Python notebooks - Jupyter and IPython

A further way to use python is through a python notebook such as `jupyter`.
This gives you a frontend application to interact with python. There's more on
this topic in the [Jupyter Section](../jupyter/README.md).

Hello World - Output
--------------------

A simple example to output the string "Hello World!" to the terminal is given
in `hello.py`. The file contents are as follows:

```python
#!/usr/bin/env python3

print("Hello World!")
```

As this file is already marked as executable, you can run it by typing
`./hello.py`.

Here we have used the
[`print()`](https://docs.python.org/3/library/functions.html#print) function,
which outputs its arguments to stdout. Note `print()` with no argument can
be used to output an empty line.

### Exercise

Create a python script called `name.py` that will output your name.

Variables, Strings and IO
-------------------------

Variables in python are implicitly typed. This means that, as with e.g. a bash
script, you can assign a value to a variable without needing to declare that
this variable holds this type of data as you need to in e.g. C/C++. Variable
types will be changed automatically when a different type of data is assigned
to them.

The previous `hello.py` example can be rewritten as `hello2.py` with:

```python
#!/usr/bin/env python3

outstring = "Hello World!"
print(outstring)
```

Here when we use the `=` symbol to assign the string "Hello World" to
`outstring`, the python interpreter understands that `outstring` should be a
string.

The `print()` function can take several arguments. We can also concatenate
strings using `+`. So we could do any of the following shown in `hello3.py`:

```python
#!/usr/bin/env python3

string1 = "Hello"
string2 = "World!"
# A comma can be used to output several items together.
# By default this inserts a space between them on output.
print(string1, string2)
# We can concatentate strings with `+`. Note we need to include the space.
string3 = string1 + " " + string2
print(string3)
# We can also do this directly in the argument of the print function.
print(string1 + " " + string2)
# Comments start with a # symbol if you haven't noticed yet. Anything after
# a # is ignored when the code is run.
```

Input
-----

We can read input from stdin with the
[`input()`](https://docs.python.org/3/library/functions.html#input) function.
An example is given in `input.py`:

```python
#!/usr/bin/env python3

prompt = "Please enter some text: "
instring = input(prompt)
# This is the same as doing
# instring = input("Please enter some text: ")
print("The text you entered was:", instring)
```

Basic Types
-----------

As discussed previously, variables are implicitly typed, with the type set
when a value is assigned. You should still understand the basic types a
variable can have. These, along with the function to explicitly generate that
type, are listed in the following table:

| Type    | Description                                         | Function |
|---------|-----------------------------------------------------|----------|
| bool    | Boolean - `True` or `False`                         | [`bool()`](https://docs.python.org/3/library/functions.html#bool) |
| int     | Integers                                            | [`int()`](https://docs.python.org/3/library/functions.html#int) |
| float   | Floating point - real numbers                       | [`float()`](https://docs.python.org/3/library/functions.html#float) |
| complex | Complex numbers - use j to represent imaginary part | [`complex()`](https://docs.python.org/3/library/functions.html#complex)
| string  | String of text                                      | [`str()`](https://docs.python.org/3/library/functions.html#func-str) |

If you want to check the type of a variable you can use the
[`type()`](https://docs.python.org/3/library/functions.html#type) function.
More details on built-in types are given the [library reference
page](https://docs.python.org/3/library/stdtypes.html#built-in-types).

### Complex numbers

A short script giving some examples of working with complex numbers is given
in `complex.py`:

```python
#!/usr/bin/env python3

a = 1.0 + 3.0j
# We could also write "a = complex(1.0, 3.0)"
b = a**0.5

# We remove the default space between arguments in this print() with sep=''.
print("a, b: ", a, ", ", b, sep='')
print("Square root of b:", b**2)
c = b.real # We could use an intermediate variable if we wanted.
print("Real part of b:", c)
print("Imaginary part of a:", a.imag)
c = a.conjugate()
print("Complex conjugate of a:", c)
```

Mathematical Operations
-----------------------

As mentioned earlier, you can perform many simple mathematical operations in
python. Here's a list of common binary operations in decreasing order of
precedence:

| Symbol              | Operation                                           |
|---------------------|-----------------------------------------------------|
| `()`                | Parentheses                                         |
| `**`                | Exponentiation                                      |
| `*`, `/`, `//`, `%` | Multiplication, division, floor division, remainder |
| `+`, `-`            | Addition and subtraction                            |

Operators at the same level are evaluated from left to right, except for
exponentiation which is from right to left. For example `7/3*4` is evaluated
as `(7/3)*4`, while `2**3**2` is evaluated as `2**(3**2)`.

Note that the floor division operator was introduced in python version 3.5 and
is not available in earlier versions (with division of integers producing the
floor of the division in these versions).

Try opening an interactive python session now and examine how these operations
work on different types of numbers. In particular compare the `/` and `//`
operators and the types of numbers they produce, and see if you can find where
the remainder operator `%` differs from the mathematical definition of the
modulus.

Some examples of arithemetic calculations are given in `arithmetic.py`. Take a
look at this file, and see if you can predict what the output will be:

```python
#!/usr/bin/env python3

result = 10/3
print(result)
print(10//3)
print(result)
result = 2 / 3**(1/2)
print(result)
print(2**(1/2))
print(2 + 3*5/2%3)
```

Compound Assignments
--------------------

Compound assignments are a shorthand way of modifying the value of a variable.
For example, the statement `count = count + 1`, could be shortened to `count
+= 1`. All the binary mathematical operators listed above can be combined with
`=` to produce a compound assignment statement. So we could write `prod = prod
* a` as `prod *= a`, for example.

More Advanced Math - Python Packages
------------------------------------

One of the main advantages of python is the huge range of packages that are
available for it. If you've installed the full Anaconda distribution, you'll
already have all the packages necessary for this course. Otherwise, packages
can be installed through your OS package manager. A large number of packages
are available as part of the [python standard
library](https://docs.python.org/3/library/). When one of these is imported
into a python code it can make a number of new functions, constants and/or
types available for use.

Packages, or modules, can be imported into a python code by entering, for
example, `import math`. The [`math`
module](https://docs.python.org/3/library/math.html) provides access to a
large set of basic mathematical functions, such as `math.log()`, `math.sin()`,
`math.erf()`, etc. An example is given in `mathops.py`:

```python
#!/usr/bin/env python3

import math as m
# This allows us to refer to functions as e.g. m.sin instead of math.sin

print(m.pi)
print(m.sin(m.pi / 2))
print(m.cos(m.pi / 2))
print(m.sin(0))
print(m.log(1.0))
```

Note: for functions that will work with complex numbers, the [`cmath`
module](https://docs.python.org/3/library/cmath.html) can be used.

If we only need to use one or two functions from a module, then we could
import these as shown in the example `mathops2.py`:

```python
#!/usr/bin/env python3

from math import sin, pi

print(pi)
print(sin(pi / 2))
print(sin(0))
```

It is also possible to import everything defined in a package with e.g. `from
math import *` but this in not generally considered good practice.

Branching - if statements
-------------------------

[`if`](https://docs.python.org/3/reference/compound_stmts.html#if) statements
can be used to change the flow of a code depending on the value of a variable.
The ability to branch down different paths is one of the most important
building blocks of a code. A simple example showing how to use `if` is given
in `ifelse.py`:

```python
#!/usr/bin/env python3

a = int(input("Enter an integer: "))
# input() returns a string so we convert it to an int.

if a > 10:
    print(a, "is greater than 10.")
elif a == 10:
    print(a, "is equal to 10.")
else:
    print(a, "is less than 10.")
```

Note here each section following the if statement has been indented. This is
not just for clarity. _Python programs are structured using indentation._ A
set of lines of code indented the same amount defines a block. If an
additional line was added to the end of above code and also indented four
spaces, it would only run in the case where `a` is less than 10. If it was
added unindented it would not form part of the if-else statement and would be
run regardless of the value of `a`.

Note the test for equality is `==` rather than `=`, as the latter is used to
assign a value to a variable. Other [comparison](https://docs.python.org/3/reference/expressions.html#value-comparisons)
are `!=` for not equal to, `<=` for less than or equal to, `>=` for greater
than or equal to, and `<`/`>` for less than/greater than.

Any of these can be combined with `and` and `or`, and the can also be
negated with `not`. An example of this is given in `ifelse2.py`:

```python
#!/usr/bin/env python3

a = int(input("Enter an integer: "))

if a > 10 and a%2 == 0:
    print(a, "is greater than 10 and even.")
elif a < 0 or a%3 == 0:
    print(a, "is negative or divisible by 3.")
else:
    print(a, "is none of the above.")
```

Python Data Structures - Strings and Lists
------------------------------------------

There are a quite number of built in data structures in python that allow
variables to be grouped in various ways, each with their own constraints,
advantages and disadvantages. Let's start with two of the most common ones,
strings which we have used already, and lists.

### [Strings](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str)

Before looking at these lets revisit strings again. Strings are stored as
ordered lists of characters, and python offers many ways to manipulate them.

An example showing how particular characters can be extracted from string is
given in `strings_ex.py`:

```python
#!/usr/bin/env python3

s = input("Input some text: ")
print("You input:", s)
print("The first character input is", s[0])
# Note indexing starts at 0.

# We can extract ranges of values as follows:
print("The second to fourth character are", s[1:4])
print("The last character input is", s[-1])
print("The first 3 characters are", s[:3])
print("The characters after the second are", s[2:])
print("The last 2 characters are", s[-2:])

print("Converted to uppercase:", s.upper())
print("Converted to lowercase:", s.lower())

# len(s) returns an int giving the number of characters.
print("The number of characters input is", len(s))

# Strings are immutable. The following would generate an error.
#s[2]  = 'c'
```

__Note the followin__:
- strings can be entered using either single or double quotes.
- strings are _immutable_. This means you can not change a single character in
  a string by `s[2] = 'c'` or similar. You need to assign a whole string at
  once.
- `s.upper()` and `s.lower()` in the above example are what is known as
  methods. You can think of these as functions that are built in to perform
  some useful operation on certain types. The string type has a long list of
  supported methods [listed
  here](https://docs.python.org/3/library/stdtypes.html#string-methods).

### Lists

Lists are one of the most useful and flexible compound types. A list contains
some sequence of things. They are _ordered_ and _mutable_. An example showing
how these can be initialized and used is in `lists_ex.py`.

```python
#!/usr/bin/env python3

# lists can be created using []. Elements can be of any type
l1 = [0, 2, 3, 4, "a", -5.5]

# Elements can be extracted in the same way as was shown for strings.
print("The first element is", l1[0])
print("The last two elements are", l1[-2:])

# The len() function works in the same way also.
print("The number of values is", len(l1))

# We can also concatenate as with strings.
print([3, 4] + l1)

# lists are mutable, so we can change values as we like.
l1[-1] = 'hello'
print("The last element is now", l1[-1])
l1[:3] = [1, -1, 0]
print("The first four elements are now", l1[:4])

# We can also easily test whether a list contains a certain value
if 1 in l1:
    print("Yes,", 1, "is in l1.")
else:
    print("No,", 1, "is not in l1.")
```

In the above example we have used
[`in`](https://docs.python.org/3/reference/expressions.html#in). This can be
used on most compound types to test whether it contains an item.

Loops - while and for Statements
--------------------------------

Looping is the other core building block of a code, alongside branching.
Statements that define groups of other statements and control the flow of a
code are called [compound
statements](https://docs.python.org/3/reference/compound_stmts.html).

### while
[`while`](https://docs.python.org/3/reference/compound_stmts.html#the-while-statement)
statements define a set of commands that will be repeated as long as an
expression is true. A simple of example of this is given in `while_ex.py`:

```python
#!/usr/bin/env python3

n = int(input("Enter a positive integer: "))

# We could use while to ensure input fits a certain criteria.
while n <= 0:
    print(n, "is not a positive integer.")
    n = int(input("Enter a positive integer: "))

factorial_n = 1
ii = 1
# Now lets use a while loop to calculate n!.
while ii <= n:
    # We can have the same variable appearing on both the left and right of
    # an assignment. The right hand side is evaluated before the assignment.
    factorial_n = factorial_n * ii
    ii = ii + 1
print(n, "! = ", factorial_n, sep='')
```

#### Task

Write a python script that prompts a user to enter an even, positive integer
and uses a while loop to repeatedly ask for this until one is entered.

### for
[`for`](https://docs.python.org/3/reference/compound_stmts.html#the-for-statement)
statements are used to create a loop that iterates over the elements of a
sequence. An example of the use of `for` is given in `for_ex.py`:

```python
#!/usr/bin/env python3

animals = ["cat", "dog", "mouse", "rabbit"]
for a in animals:
    print(a, "has", len(a), "letters.")

scores = [60, 65, 74, 62, 58, 80]
total = 0
for score in scores:
    total = total + score
print("The average score is", total/len(scores))
```

It's often very useful to pair `for` with
[`range()`](https://docs.python.org/3/library/stdtypes.html#range), which
can produce a sequence of numbers automatically. An example of this is
given in `for_ex2.py`:

```python
#!/usr/bin/env python3

items = [1, "a", 3, "apple"]
# range(4) produces the numbers 0, 1, 2, 3, so they can be used directly as
# indices of compound types such as strings or lists.
for ii in range(len(items)):
    print("Element", ii, "is", items[ii])

n = int(input("Enter a positive integer: "))
# Now lets use a for loop to calculate n!.
factorial_n = 1
for ii in range(1, n+1):
    factorial_n = factorial_n * ii
print(n, "! = ", factorial_n, sep='')
```

Note `range(n)` will yield numbers from `0` through to`n-1` so if we want
numbers from `1` to `n` we need to specify both the start and finish values
as `range(1, n+1)`.

#### Task

Write a python script that reads a number `n` and outputs the sum of all the
numbers from `1` to `n`, calculated using a for loop.

Do you know an alternative way to calculate the result?

### Other Useful Statements for Loops: break, continue, enumerate

#### break
The
[`break`](https://docs.python.org/3/reference/simple_stmts.html#the-break-statement)
statement can be used to _break_ out of a loop before the full number of
iterations are completed. Both
[`while`](https://docs.python.org/3/reference/compound_stmts.html#the-while-statement)
and
[`for`](https://docs.python.org/3/reference/compound_stmts.html#the-for-statement)
loops support the use of an additional `else` clause. If the loop completes
the full number of iterations, the code in the `else` clause is executed.
However, if the loop is broken out of using `break`, then the code in the
`else` clause is skipped. An example using `break` and the loop `else` to test
if a number is prime is given in `break_ex.py`:

```python
#!/usr/bin/env python3

n = int(input("Please enter a positive integer: "))
for ii in range(2, n):
    if n % ii == 0:
        print(n, "is not prime.")
        break
else:
    print(n, "is prime.")
```

#### continue
The
[`continue`](https://docs.python.org/3/reference/simple_stmts.html#the-continue-statement)
statement can also be used to affect loops. If a `continue` statement is
reached in a loop, the loop moves straight to the next iteration, skipping any
remaining statements. The same functionality could be achieved by having a big
if-else statement in the body of the loop, but using continue instead can make
your easier to read, and it's easier to add in later if you realise there are
cases you want to skip. An example is given in `continue_ex.py`:

```python
#!/usr/bin/env python3

a = [4, 3, 1, 4, 0, 2, 0, 5, 2, 8]
invsum = 0.0
for ii in a:
    if ii == 0:
        continue
    invsum += 1 / ii
print("The sum of the inverses of the non-zero elements is", invsum)
```

#### enumerate

[`enumerate`](https://docs.python.org/3/library/functions.html#enumerate) can
be used to return both the index and the value of an item in a sequence. This
is often useful in `for` loops. It can be used as follows:

```python
#!/usr/bin/env python3

letters = ["Alpha", "Beta", "Gamma", "Delta"]
for ii, val in enumerate(letters):
    print("The value at index", ii, "is", val)
```

More Compound Types - sets, tuples and dicts
--------------------------------------------

There are several more compound types that are commonly used. They are the
[set type](https://docs.python.org/3/library/stdtypes.html#set-types-set-frozenset),
the [tuple type](https://docs.python.org/3/library/stdtypes.html#tuples) and
the [dict type](https://docs.python.org/3/library/stdtypes.html#mapping-types-dict).

An example script showing how these types may be initialized and used with a
for loop is given in `more_types.py`:

```python
#!/usr/bin/env python3

# Lists are mutable ordered sequences of variables of any type.
options_list = ["a", "b", "c", "d", 5, -6]
for option in options_list:
    print(option)
print()

# Sets are mutable unordered groups of data with unique immutable elements.
options_set = {"a", "b", "c", "d", 5, -6}
for option in options_set:
    print(option)
print()
# Note there is also the frozenset type if you need to create immutable sets.

# Tuples are immutable ordered groups of values formed by a comma separated
# list of values.
options_tuple = "a", "b", "c", "d", 5, -6
# We could also have written this as
# options_tuple = ("a", "b", "c", "d", 5, -6)
for option in options_tuple:
    print(option)
print()
# Tuple expressions are often used to assign several values at once, or when
# a function returns several values at once. E.g.:
a, b, c = 1, 2, 3
print(a, b, c)
print()

# Dicts are groups of key: value pairs. The keys must be unique and immutable,
# but the dict is mutable. They are unordered.
info = {"Alice": 1, "Bob": 0, "Carol": 0.5}
info["Carol"] = 0.6
for name in info:
    print(name, info[name])
```

Functions
---------

As your codes get bigger, it will be useful to break it up into smaller
pieces, each of which would be specialised to perform a certain task. This
makes a code much easier to read and understand, and to debug. In python
this is easily done using functions. These can also reduce the amount of code
you have to write, as once a function has been defined, it can be called as
many times as needed.

A simple example where we use a function to output the string "Hello
World!" is given in `func_hello.py`:

```python
#!/usr/bin/env python3

def hello():
    """Output the text 'Hello World!'."""
    print("Hello World!")

# We can call this function by entering its name.
hello()
hello()
```

Here, the keyword
[`def`](https://docs.python.org/3/reference/compound_stmts.html#def) is used
to declare a function. This is followed by the name we're going to use for the
function, with any arguments the function will use included in the trailing
parentheses, followed by a colon `:`. In this first example we didn't have any
function arguments. The body of the function is then indented as is usual for
defining a section of code in python. Note in the first line of the function
definition we have placed a line describing what the function does between
triple quotes. This is called a docstring and we'll come back to this later.

To use the function, we can call it with `Hello()` anywhere after the function
definition.

Another example, where we use a function to calculate the average of sequence
of numbers is shown in `func_average.py`:

```python
#!/usr/bin/env python3

def average(nums):
    """Return the average of a group of numbers."""
    return sum(nums)/len(nums)

test_nums = 1, 2, 3, 4, 5
print("The average of these numbers is", average(test_nums))
```

You'll notice a few differences here with respect to the first example.
- The function now takes an argument `nums`. This allows us to pass references
  to objects into the function.
- It now has a line that begins with
  [`return`](https://docs.python.org/3/reference/simple_stmts.html#return).
  This allows you to use the result of a function elsewhere in your code. In
  the above example we used this directly in the `print()` function, but we
  could also have assigned this to a variable.
- We have used the built-in
  [`sum()`](https://docs.python.org/3/library/functions.html#sum) function.
  This returns the sum of the values of any group of items. It will work for
  lists, sets, tuples and even statements like `range`, so we could for
  example, calculate the sum of values from `1` to `n` as `sum(range(1,
  n+1))`.

Note: functions can have other functions defined within them.

### Boilerplate, Modules, and using a main() Function

A slightly modified version of the previous example is given in
`func_average2.py`:

```python
#!/usr/bin/env python3

def average(nums):
    """Return the average of a group of numbers."""
    return sum(nums)/len(nums)

def main():
    test_nums = 1, 2, 3, 4, 5
    print("The average of these numbers is", average(test_nums))

if __name__ == '__main__':
    main()
```

This is the standard way to construct a python code, and you'll often see
this in examples of python script elsewhere. Here we have a created a function
called main which contains the code we have in the main body previously,
and we have an if statement at the end, which performs a special action.
When a python code is loaded, a number of internal variables are set. One of
these is `__name__`. If the code is executed directly, the value of this is
set to `__main__`. However, if the code is imported, this is instead set to
the name of the module. So in the latter case, the `main()` function is
_not_ called. This allows us to import our python code as a _module_ and use
our functions in other python scripts.

### Interactive Use
This is also useful, as we can import our scripts into an interactive
python session and test our functions directly. Try this now:
- Open an interactive python session by typing `python3` in a terminal from
  the directory containing the `func_average.py` and `func_average2.py`
  scripts.
- Import the first script by typing `import func_average`.
    - You'll see that this outputs the code that was in the main body as
    soon as it is imported.
    - Create a list of value such as `a = [0, 3, 4, 10]` and find the average
    by typing `func_average.Average(a)`
- Import the second script by typing `import func_average2`.
    - This time, the code contained in the main function was not executed.
    - We could use this version of the average function by calling
    `func_average2.Average(a)`.
    - Now we can also call the main function by typing `func_average2.main()`
- Try typing `help(func_average2.Average)`. You'll see the
  [`help()`](https://docs.python.org/3.6/library/functions.html#help) function
  loads a description of the function as given in the docstring. This will
  usually use the `less` pager. You can leave the documentation page by
  pressing `q`.
- If we wanted to modify the code containing the function, say if we were
  debugging it, we could do so and reload it in the interactive session using
  the
  [`reload()`](https://docs.python.org/3/library/importlib.html#importlib.reload)
  command from the
  [`importlib`](https://docs.python.org/3/library/importlib.html) module. Try
  this by first typing `from importlib import reload` followed by
  `reload(func_average)`.
- We could also construct a python code consisting only of functions and no
  main function or code outside of functions. Running this as a script
  wouldn't do anything, but the code could be loaded as a module in other
  python scripts or in interactive sessions. For this reason, there is no need
  to start such files with a `#!` line.

### General Advice on Functions

- Try to keep functions fairly short. It makes your code much easier to read
  if your functions are short enough that you can see the whole function at
  once on your screen.
- Try to keep functions specialised. Break up your problem into small chunks
  where you can use functions that do one thing well. This will make it much
  easier to debug your code when something inevitably doesn't work as
  expected.
- Use the construction given in `func_average2` where any main body of the
  code is contained in a `main()` function. This will make it easier to test
  your code and expand it later should you desire.
- Always use a **docstring**. In short codes, or for fairly straightforward
  functions, this can be a brief one-line description. For more complex codes
  it is worth writing more detailed, multi-line descriptions of what functions
  do. These should still begin with a single summary line. This would be
  followed by an empty line, and then several lines giving details of the
  function, what arguments it expects, etc. Python docstring conventions are
  described in [PEP 257](https://www.python.org/dev/peps/pep-0257/).

Modules
-------

As mentioned earlier, you now know everything you need to make your own
modules. To create a module, you create a `.py` file named however you want
your module to be named. This can then be import into other python files with
the `import` command. For example if had two functions, `func1` and `func2` in
a file called `mymodule.py`, you could use these in another python code with
`import mymodule`. The functions could be called with `mymodule.func1()` and
`mymodule.func2()`.

An example code which imports the `func_average2` module created earlier is
given in `module_ex.py`:

```python
#!/usr/bin/env python3

import func_average2

def text_to_floats(line):
    """Convert a string containing numbers to a python list of floats."""
    # First convert any commas or tabs to spaces
    line = line.replace(',', ' ')
    line = line.replace('\t', ' ')
    line_vals = []
    # We need to initialize an empty list before we can add to it.
    for x in line.split():
        # The .split() method breaks up a string based on spaces by default.
        line_vals.append(float(x))
        # We convert each item to a float and append it to our list.
    return line_vals

while True:
  input_nums = input("Enter some numbers: ")
  # We keep looping until no text is entered.
  if input_nums == "":
      break
  num_list = text_to_floats(input_nums)
  print("The average of these is", func_average2.average(num_list))
  # The above calls the "average" function from the "func_average2" module.
```

Coding Style
------------

Now that you've seen the basics, it's good to take some time to look at how
you write your code. Python actually has an official style guide:
[PEP 8](https://www.python.org/dev/peps/pep-0008/). I suggest you take the
time to read this, and do your best to follow these guidelines in all the
code you write. Some of the more important suggestions are:
- Always use four spaces for indentation and never use tabs.
- Keep lines under 79 characters in length. If you're using vim you could add
  the line `set cc=80` to your `~/.vimrc` file to give you a vertical bar at
  column 80 you can use as a guide. If you need to write a line longer than
  this you can use parentheses.
- Separate function definitions with empty lines.
- Put spaces after commas, and put space around binary operators in a way that
  makes sense, such as by putting spaces around operators with lower
  precedence to make the order of evaluation clearer, such as `a + b**2`.

Working with Files
------------------

Generally, you don't want whoever is using your code to have to type in the
numbers at the terminal each time your code is run. Nor do you want them to
have to copy output from the terminal screen if your producing some data
for plotting or as an intermediate step for other analysis. This is why it's
important to be able to read from and write to file from your code.

### Reading Data from Files

Before you can read from a file, you need to tell python to open the file,
using the [`open()`](https://docs.python.org/3/library/functions.html#open)
function. This returns a [file object which allows you to interact with the
file in various ways](https://docs.python.org/3/library/io.html). Then the
file could be read in its entirety with the `read()` method, or the lines of
the file could be iterated over in a for loop for example. This is shown in
the file `read_ex.py` which outputs the lines of `test.txt` using both
these approaches:

```python
#!/usr/bin/env python3

f = open("test.txt")

# This assigns the entire contents of the file to "fullfile"
fullfile = f.read()
print("The file contents are:")
print(fullfile)
print()

# As we read a file, the current position is tracked. After reading the whole
# file, we are positioned at the end, and we need to return to the start if
# we want to re-read the file. We can do this using the seek() method.
f.seek(0)
print("Outputting one line at a time:")
for line in f:
    # We add end='' to suppress the default newline that's output at the end
    # of the print function. There's already a newline at the end of each line
    # of the file.
    print(line, end='')

# And we should make a habit of closing the file once we've finished.
f.close()
```

The file `example.dat` contains some output from a calculation. The file
`read_ex2.py` gives an example of how we can read numbers from this file
and do something with them:

```python
#!/usr/bin/env python3

f = open("example.dat")

totals = [0, 0]
count = 0
for line in f:
    count += 1
    # Each line is read as a string.
    # split() is a string method that outputs each space-separated item.
    for ii, val in enumerate(line.split()):
        # Remember we need to convert the value from a string to a number
        # before we can add it.
        totals[ii] += float(val)
# We can close the file as soon as we don't need it any more.
f.close()

for ii, total in enumerate(totals):
    # Recall python list indices start from 0, so we add 1 here.
    print("The average of column", ii+1, "is", total/count)
```

### Writing Data to Files

Data can be written to a file in a similar way. However, when we open the file
we need to use an additional argument to flag that we want to be able to write
to it. See the [`open()
documentation`](https://docs.python.org/3/library/functions.html#open) for
details of the other possibilities here. And to actually write data to the
file we use the
[`write()`](https://docs.python.org/3/library/io.html#io.RawIOBase.write) file
method. There are a few important things to remember when doing this:
- The argument to `write()` should be a string, in contrast to `print()` which
  can figure out what to do with whatever you pass to it.
- Again in contrast to `print()`, it will not output a trailing newline by
  default
- It will return the number of bytes of data written (number of characters)
  for text. You can safely ignore this, though you may see this if you're
  writing to file from an interactive session.

An example where the [random
module](https://docs.python.org/3/library/random.html#module-random) is also
used, which outputs 100 random numbers to a file, is given in `write_ran.py`:

```python
#!/usr/bin/env python3

# This python module is used for generating random numbers.
import random

# Note we add the additonal argmument "w" when we want to write to a file.
f = open("100randoms.dat", "w")

for ii in range(100):
    # random.gauss picks a random number using a gaussian distribution
    # centred at the first argument and a width given by the second argument.
    n = str(random.gauss(100.0, 10.0))
    f.write(str(n) + '\n')
    # Alernatively we could use the 'file=f' option to print as follows
    # print(n, file=f)

f.close()
```

An example where this has been re-written to use functions is given
in `write_ran2.py`:

```python
#!/usr/bin/env python3

import random

def write_to_file(filename, data):
    """Output the elements of data to the file filename one per line."""
    f = open(filename, "w")
    for item in data:
        f.write(str(item) + '\n')
    f.close()

def gen_random_data(n, centre, width):
    """Return n random numbers from a gaussian using centre and width."""
    random_data = []
    for ii in range(n):
        random_data.append(random.gauss(centre, width))
    return random_data

def main():
    write_to_file("200randoms.dat", gen_random_data(200, 10, 1))

if __name__ == '__main__':
    main()
```

If you want to read or write to a "standard" format such as
[csv](https://docs.python.org/3/library/csv.html),
[xml](https://docs.python.org/3/library/xml.html),
[json](https://docs.python.org/3/library/json.html), there are often modules
available that will make reading and writing these files very straightforward.
