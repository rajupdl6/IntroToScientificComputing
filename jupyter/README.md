Jupyter
=======

[Full topic list](../README.md)

`jupyter` offers a graphical notebook style interface to `ipython` (a command
shell for python) functioning in a similar way to Mathematica. This offers
many more features than simply using python interactively in a terminal.

Installation
------------

You can install `jupyter` using the python package manager `pip`. If you're
on an Ubuntu or Debian system you can do this system-wide for python3 with

```bash
sudo pip3 install --upgrade pip
sudo pip3 install jupyter
```

If you're using the TSM Linux VM I suggest you do this now.

Starting Jupyter
----------------

You can start `jupyter` by typing ``jupyter-notebook`` in a terminal.
Alternatively, depending on how you installed it, you may need to type
``jupyter notebook``. This will start a jupyter server in the terminal, and
open the server interface as a web page on your default browser that will
allow you to interact with it. An example of what you will see in your browser
when you first start jupyter in an empty folder is shown below: ![jupyter
browser interface](jupyter1.png).

You can do several things from this web interface. If you click the "new"
dropdown menu, you will see options to create a new text file or folder, or to
start a terminal, which gives you a terminal in your browser, followed by a
list of notebooks which can be started, which will likely just list "Python 3"
by default. Click this now to start a new python 3 notebook. The notebook
interface will be started in a new browser tab. How this looks is shown below:
![jupyter notebook](jupyter2.png).

The notebook interface offers many nice features. For example, it
automatically saves a checkpoints your work, will automatically close
brackets, and it can display graphics directly in the notebook. The input and
output is organised around cells, where you can enter commands in a similar
way as with an interactive python session, but you can edit and rearrange
cells easily. To execute the commands contained in a cell, press
``shift+enter`` as the enter key on its own will give you a new line in the
current cell allowing you to enter several lines of code to be executed
together. There are also menu buttons for many operations, such as executing
the current cell. You can hover over them to see what function they perform.

Each notebook you open from the server interface will have its own kernel that
runs in the server. This means that any function or variable definitions or
imports are local to the notebook, and will be available as long as the kernel
is running. When a notebook is saved, you can close the browser tab where it
is running and reopen it again to carry on from where you were. There is a
separate option to stop the kernel for a running notebook from the server
interface page. And if you close the browser interface page, the server will
still be running, so you can usually get back to it by visiting
[http://localhost:8888](http://localhost:8888). To shut down the server, go to
the terminal where you started it, and press ``ctrl+c``.

Shortcuts and Cell Types
------------------------

`jupyter` has many shortcuts for e.g. switching between input cell types, or
selecting, running, merging or deleting cells. You can bring up a list of
these by first pressing `Escape` to ensure you're in command mode (the cursor
will not be active in any cell), and then pressing `h`. It will take a while
to get to know them all, so I suggest trying to remember a few at a time as
you go. You can perform most operations using the mouse and menus so the
shortcuts are not mandatory but will make you more efficient.

There are three types of cell you can switch between:

1. Code (`Y`) - for entering sections of code of whatever language you've
   chosen for the notebook. We've installed `jupyter` for python3 but many
   other languages are possible.
2. Markdown (`M`) - for entering sections of text, with desired formatting
   indicated by some simple rules. LaTex style mathematical expressions are
   also supported.
3. Raw (`R`) - raw text with any highlighting or formatting disabled.

There is an example notebook given in the [`Example`](Example) directory that
contains a markdown cell and a few code cells. Launch `jupyter` and open this
to take a look. Try entering some code and markdown cells yourself.

Full List of Shortcuts
----------------------

Note shortcuts are not case sensitive.

- Command Mode (`Esc`)
    - `F`: find and replace
    - `Ctrl+Shift+F`: open the command palette
    - `Ctrl+Shift+P` : open the command palette
    - `Enter` : enter edit mode
    - `P` : open the command palette
    - `Shift+Enter` : run cell, select below
    - `Ctrl+Enter` : run selected cells
    - `Alt+Enter` : run cell, insert below
    - `Y` : to code
    - `M` : to markdown
    - `R` : to raw
    - `1` : to heading 1
    - `2` : to heading 2
    - `3` : to heading 3
    - `4` : to heading 4
    - `5` : to heading 5
    - `6` : to heading 6
    - `K` : select cell above
    - `Up` : select cell above
    - `Down` : select cell below
    - `J` : select cell below
    - `Shift+K` : extend selected cells above
    - `Shift+Up` : extend selected cells above
    - `Shift+Down` : extend selected cells below
    - `Shift+J` : extend selected cells below
    - `A` : insert cell above
    - `B` : insert cell below
    - `X` : cut selected cells
    - `C` : copy selected cells
    - `Shift-V` : paste cells above
    - `V` : paste cells below
    - `Z` : undo cell deletion
    - `D,D` : delete selected cells
    - `Shift+M` : merge selected cells, or current cell with cell below if only one cell selected
    - `Ctrl+S` : Save and Checkpoint
    - `S` : Save and Checkpoint
    - `L` : toggle line numbers
    - `O` : toggle output of selected cells
    - `Shift+O` : toggle output scrolling of selected cells
    - `H` : show keyboard shortcuts
    - `I`,`I` : interrupt kernel
    - `0`,`0` : restart the kernel (with dialogue)
    - `Ctrl+V` : Dialogue for paste from system clipboard
    - `Esc` : close the pager
    - `Q` : close the pager
    - `Shift+L` : toggles line numbers in all cells, and persist the setting
    - `Shift+Space` : scroll notebook up
    - `Space` : scroll notebook down
- Edit Mode (`Enter`)
    - `Tab` : code completion or indent
    - `Shift+Tab` : tooltip
    - `Ctrl+]` : indent
    - `Ctrl+[` : dedent
    - `Ctrl+A` : select all
    - `Ctrl+Z` : undo
    - `Ctrl+Shift+Z` : redo
    - `Ctrl+Y` : redo
    - `Ctrl+Home` : go to cell start
    - `Ctrl+Up` : go to cell start
    - `Ctrl+End` : go to cell end
    - `Ctrl+Down` : go to cell end
    - `Ctrl+Left` : go one word left
    - `Ctrl+Right` : go one word right
    - `Ctrl+Backspace` : delete word before
    - `Ctrl+Delete` : delete word after
    - `Ctrl+M` : command mode
    - `Ctrl+Shift-F` : open the command palette
    - `Ctrl+Shift-P` : open the command palette
    - `Esc` : command mode
    - `Shift+Enter` : run cell, select below
    - `Ctrl+Enter` : run selected cells
    - `Alt+Enter` : run cell, insert below
    - `Ctrl+Shift+Minus` : split cell
    - `Ctrl+S` : Save and Checkpoint
    - `Down` : move cursor down
    - `Up` : move cursor up

Magics
------

Jupyter, when used with the ipython kernel, has a long list of special built
in commands it understands that allow you to do some fairly complex things
with a single command. These are divided into line magics, which perform or
wrap a single command, and cell magics which apply to an entire cell. Line
magics are usually preceded by a `%` symbol (although there is a setting called
Automagic which is usually enabled and allows them to be entered without
this), while cell magics are preceded by `%%`. Here are a few useful ones:
- `%lsmagic` - list all magic commands. This will also tell you if Automagic
  is enabled.
- '%who' - list all the interactive variables. Additionally `%who_ls` will
  return the variables as a sorted list, and `%whos` will return some
  additional information about each variable.
- '%system` or `%sx` - execute a system command. For exampled `%sx ls` will
  list all files in the current directory. This can also be done by
  beginning a line with `!!`, e.g. `!!ls`.
- `%time` or `%%time` - time the execution of an expression or cell, returning
  both the cpu time and wall time.
- '%timeit` or `%%timeit` - analyse the time taken for the execution of
  an expression or cell. This will perform the expression a number of times
  and return some statistics about the time taken.
